
self.addEventListener('message', function(e) {
	//lineMaker(e.data.add, e.data.img, e.data.width, e.data.height);
	spiral(e.data.x, e.data.y, e.data.b, e.data.samples, e.data.img, e.data.width, e.data.height, e.data.color, e.data.full);
}, false);

/* returns average color around x,y in radius */
function getAverage(x, y, radius, image, width, height) {
	var pixels = [];
	/* change this so it's not a box probably */
	for(var i = Math.round(y - radius); i <= Math.round(y + radius); i++) {
		for(var j = Math.round(x - radius); j <= Math.round(x + radius); j++) {
			if(i >= 0 && i < height && j >= 0 && j < width) {
				pixels.push(image[i * width + j]);
			}
		}
	}
	if(pixels.length == 0) {
		return null;
	}
	var sum = pixels.reduce(function(a, b) {
		return a + b;
	}, 0);
	return Math.round(sum / pixels.length);
}

function getColorAverage(x, y, radius, image, width, height) {
	var pixels = [];
	/* change this so it's not a box probably */
	for(var i = Math.round(y - radius); i <= Math.round(y + radius); i++) {
		for(var j = Math.round(x - radius); j <= Math.round(x + radius); j++) {
			if(i >= 0 && i < height && j >= 0 && j < width) {
				pixels.push([image[(i * width + j) * 4], image[(i * width + j) * 4 + 1], image[(i * width + j) * 4 + 2]]);
			}
		}
	}
	if(pixels.length == 0) {
		return null;
	}
	var sum = pixels.reduce(function(a, b) {
		return [a[0] + b[0], a[1] + b[1], a[2] + b[2]];
	}, [0,0,0]);
	return [Math.round(sum[0] / pixels.length), Math.round(sum[1] / pixels.length), Math.round(sum[2] / pixels.length)];
}

/* should be returning array of magnitudes, x,y is starting point 
	r = b * theta */
function spiral(x, y, b, samples, image, width, height, color, full) {
	var mags = [];
	var theta = Math.PI/2;
	var r = b * theta;
	
	// determine how far the spiral will go
	if(full) {
		var candidates = [Math.sqrt(x * x + y * y), Math.sqrt((width - x) * (width - x) + y * y), Math.sqrt(x * x + (height - y) * (height - y)), Math.sqrt((width - x) * (width - x) + (height - y) * (height - y))];
		var diag = Math.max.apply(null, candidates);
	} else {
		var candidates = [x, width - x, y, height - y];
		var max = Math.max.apply(null, candidates);
		candidates.splice(candidates.indexOf(max), 1);
		var diag = Math.max.apply(null, candidates);
	}
	
	while(r <= diag) {
		var coords = polarToCart(r, theta);
		if(color) {
			var avg = getColorAverage(coords[0] + x, coords[1] + y, b, image, width, height);
		} else {
			var avg = getAverage(coords[0] + x, coords[1] + y, b, image, width, height);
		}
		mags.push(avg);
		theta += 1/(samples * r);
		r = b * theta;
	}
	postMessage(mags);
	close();
}

/**/
function polarToCart(r, theta) {
	return [r * Math.cos(theta), r * Math.sin(theta)];
}
