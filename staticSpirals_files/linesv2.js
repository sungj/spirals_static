/* polyfill for canvas.toBlob() from https://developer.mozilla.org/en-US/docs/Web/API/HTMLCanvasElement/toBlob */
if (!HTMLCanvasElement.prototype.toBlob) {
	Object.defineProperty(HTMLCanvasElement.prototype, 'toBlob', {
		value: function (callback, type, quality) {
			var binStr = atob( this.toDataURL(type, quality).split(',')[1] ),
				len = binStr.length,
				arr = new Uint8Array(len);
			for (var i=0; i<len; i++ ) {
				arr[i] = binStr.charCodeAt(i);
			}
			callback( new Blob( [arr], {type: type || 'image/png'} ) );
		}
	});
}

function process(img, color) {
	ds.style.display = 'none';
	
	if(workers.length != 0) {
		for(var i=0; i<workers.length; i++) {
			workers[i].terminate();
		}
		workers = [];
	}
	
	var canvas = document.createElement('canvas');
	var context = canvas.getContext('2d');
	if(img.width > img.height) {
		canvas.width = 1024;
		canvas.height = Math.floor(img.height/img.width * 1024);
	} else {
		canvas.height = 1024;
		canvas.width = Math.floor(img.width/img.height * 1024);
	}
	lDisplay.width = canvas.width;
	lDisplay.height = canvas.height;
	cCanvas.width = canvas.width;
	cCanvas.height = canvas.height;
	cWidth = canvas.width;
	cHeight = canvas.height;
	
	context.fillRect(0,0,1024,1024);
	context.drawImage(img,0,0, canvas.width, canvas.height);
	var imageData = context.getImageData(0,0,canvas.width, canvas.height);
	var data = imageData.data;
	
	lines = [];
	linesDrawn = 0;
	
	var x = cRatioX * cWidth;
	var y = cRatioY * cHeight;
	
	lContext.lineWidth = 1.6;
	
	if(color === false) {
		var gray = new Uint8Array(data.length/4);
		// convert to grayscale
		for(var i=0; i < data.length/4; i++) {
			gray[i] = (Math.floor(0.21 * data[i*4] + 0.72 * data[i*4+1] + 0.07 * data[i*4+2])) // grayscale using CIE luminance
		}
		workers.push(new Worker('javascripts/workerv2.js'));
		
		workers[0].addEventListener('message', drawSpiral(x, y, radius, sampleRate, amplitude), false);
		workers[0].postMessage({x: x, y: y, b: radius, samples: sampleRate, img: gray, width: imageData.width, height: imageData.height, color: color, full: full});

	} else {
		workers.push(new Worker('javascripts/workerv2.js'));
		workers[0].addEventListener('message', drawSpiralColor(x, y, radius, sampleRate, amplitude), false);
		workers[0].postMessage({x: x, y: y, b: radius, samples: sampleRate, img: data, width: imageData.width, height: imageData.height, color: color, full: full});
	}
}

function drawSpiral(x, y, b, samples, amplitude) {
	return function(e) {
		rafID++;
		this.rafID = rafID; // ID to check for multiple requestAnimationFrames
		var mags = e.data;
		this.rate = mags.length/48000;
		this.lastTime = performance.now();
		this.i = 0;
		this.theta = Math.PI/2;
		this.r = b * this.theta;
		this.oldCoords = polarToCart(this.r, this.theta);
		this.b = b;
		this.samples = samples;
		this.amplitude = amplitude;
		this.x = x;
		this.y = y;
		this.flip = 1;
		this.theta += 1/(this.samples * this.r);
		this.r = this.b * this.theta;
		
		prepCanvas();
		if(add) {
			lContext.strokeStyle = 'rgb(255,255,255)';
			//lContext.globalCompositeOperation = 'lighter';
		} else {
			//lContext.globalCompositeOperation = 'darker';
			lContext.strokeStyle = 'rgb(0,0,0)';
		}
		window.requestAnimationFrame(drawGray.bind(this, mags))
	}
}

function drawGray(array, time) {
	var elapsed = time - this.lastTime;
	var numDraw = Math.ceil(elapsed * this.rate);
	if(elapsed > 0) {
		this.rate = this.rate + this.rate * elapsed * 0.0005;
	}
	this.lastTime = time;
	// since we don't need to change colors, we can begin and stroke outside the iteration loop
	lContext.beginPath();
	for(var j=0; j < numDraw; j++) {
		if(this.i < array.length && rafID == this.rafID) {
			//lContext.beginPath();
			lContext.moveTo(this.oldCoords[0] + this.x, this.oldCoords[1] + this.y);
			var coords = polarToCart(this.r, this.theta);
			if(array[this.i] != null) {
				if(add) {
					var perp = getPerp(this.oldCoords[0], this.oldCoords[1], coords[0], coords[1], array[this.i] * this.b * this.amplitude * 0.01 * this.flip);
				} else {
					var perp = getPerp(this.oldCoords[0], this.oldCoords[1], coords[0], coords[1], (255 - array[this.i])* this.b * this.amplitude * 0.01 * this.flip);
				}
				lContext.quadraticCurveTo(perp[0] + this.x, perp[1] + this.y, coords[0] + this.x, coords[1] + this.y);
				//dContext.lineTo(coords[0] + x, coords[1] + y);
				//lContext.stroke();
			}
			this.oldCoords = coords;
			this.theta += 1/(this.samples * this.r);
			this.r = this.b * this.theta;
			this.i++;
			this.flip = -this.flip
		} else {
			break;
		}
	}
	lContext.stroke();
	if(this.i < array.length) {
		window.requestAnimationFrame(drawGray.bind(this, array));
	} else {
		finishedDrawing(array);
	}
}

function drawSpiralColor(x, y, b, samples, amplitude) {
	return function(e) {
		rafID++;
		this.rafID = rafID;
		var mags = e.data;
		this.rate = mags.length/48000;
		this.lastTime = performance.now();
		this.i = 0;
		this.theta = Math.PI/2;
		this.r = b * this.theta;
		this.oldCoords = polarToCart(this.r, this.theta);
		this.b = b;
		this.samples = samples;
		this.amplitude = amplitude;
		this.x = x;
		this.y = y;
		this.flip = 1;
		this.theta += 1/(this.samples * this.r);
		this.r = this.b * this.theta;
		
		prepCanvas();
		lContext.globalCompositeOperation = 'source-over';
		window.requestAnimationFrame(drawColor.bind(this, mags))
	}
}

function drawColor(array, time) {
	var elapsed = time - this.lastTime;
	var numDraw = Math.ceil(elapsed * this.rate);
	if(elapsed > 0) {
		this.rate = this.rate + this.rate * elapsed * 0.0005;
	}
	this.lastTime = time;
	for(var j=0; j < numDraw; j++) {
		if(this.i < array.length && rafID == this.rafID) {
            lContext.beginPath();
			lContext.moveTo(this.oldCoords[0] + this.x, this.oldCoords[1] + this.y);
			var coords = polarToCart(this.r, this.theta);
			if(array[this.i] != null) {
				if(add) {
					lContext.strokeStyle = 'rgb(' + Math.round(array[this.i][0] + (255 - array[this.i][0]) * 0.2) + ',' + Math.round(array[this.i][1] + (255 - array[this.i][1]) * 0.2) + ',' + Math.round(array[this.i][2] + (255 - array[this.i][2]) * 0.2) + ')';
					var perp = getPerp(this.oldCoords[0], this.oldCoords[1], coords[0], coords[1], (array[this.i][0] + array[this.i][1] + array[this.i][2]) * this.b * this.amplitude * 0.005 * this.flip);
				} else {
					lContext.strokeStyle = 'rgb(' + Math.round(array[this.i][0] * 0.8) + ',' + Math.round(array[this.i][1] * 0.8) + ',' + Math.round(array[this.i][2] * 0.8) + ')';
					var perp = getPerp(this.oldCoords[0], this.oldCoords[1], coords[0], coords[1], (765 - array[this.i][0] - array[this.i][1] - array[this.i][2]) * this.b * this.amplitude * 0.005 * this.flip);
				}

				lContext.quadraticCurveTo(perp[0] + this.x, perp[1] + this.y, coords[0] + this.x, coords[1] + this.y);
				//dContext.lineTo(coords[0] + x, coords[1] + y);
				lContext.stroke();
			}
			this.oldCoords = coords;
			this.theta += 1/(this.samples * this.r);
			this.r = this.b * this.theta;
			this.i++;
			this.flip = -this.flip
		} else {
			break;
		}
	}
	if(this.i < array.length) {
		window.requestAnimationFrame(drawColor.bind(this, array));
	} else {
		finishedDrawing(array);
	}
}

function polarToCart(r, theta) {
	return [r * Math.cos(theta), r * Math.sin(theta)];
}

/* returns point that is perpendicular amp away*/
function getPerp(x0, y0, x1, y1, amp) {
	var dy = y1 - y0;
	var dx = x1 - x0;
	var length = Math.sqrt(dy * dy + dx * dx);
	return [(x0 + x1)/2 + dy/length * amp, (y0 + y1)/2 - dx/length * amp];
}

function finishedDrawing(array) {
	
	ds.style.display = 'block';
}

/* compress magnitude array using DCT */
function storeArray(array, blockSize = 64) {
	var runningZeroAmplitudes = [];
	// dropping last bit of array that doesn't hit blockSize
	for(var i=0; i < Math.floor(array.length / blockSize); i++) {
		var block = array.slice(i * blockSize, (i+1) * blockSize);
		
		var shifted = block.map(function(x) { // normalize
			return (x-128)/128;
		});
		fastDctLee.transform(shifted);
		var zeroCount = 0;
		for(var j=0; j < shifted.length; j++) {
			if(Math.abs(shifted[j]) < 1) {
				zeroCount++;
			} else {
				runningZeroAmplitudes.push(zeroCount);
				runningZeroAmplitudes.push(Math.round(shifted[j]));
				zeroCount = 0;
			}
		}
		// need to add final zeros
		runningZeroAmplitudes.push(zeroCount - 1);
		runningZeroAmplitudes.push(0);
	}
	var compressed = LZString.compressToEncodedURIComponent(runningZeroAmplitudes.toString());
	console.log(compressed.length);
	
	return compressed;
}

function drawStored(x, y, b, samples, amplitude, mags, blockSize = 64) {
	// probably need to set add
	var decompressed = LZString.decompressFromEncodedURIComponent(mags);
	var runningZeroAmplitudes = decompressed.split(',').map(Number);
	
	var unpackedZeroAmplitudes = [];
	for(var i=0; i < runningZeroAmplitudes.length / 2; i++) {
		for(var j=0; j < runningZeroAmplitudes[i*2]; j++) {
			unpackedZeroAmplitudes.push(0);
		}
		unpackedZeroAmplitudes.push(runningZeroAmplitudes[i*2+1]);
	}
	var reconstruction = [];
	for(var i=0; i < unpackedZeroAmplitudes.length / blockSize; i++) {
		var block = unpackedZeroAmplitudes.slice(i * blockSize, (i+1) * blockSize);
		fastDctLee.inverseTransform(block);
		var original = block.map(function(x) {
			return x / blockSize * 2 * 128 + 128;
		});
		reconstruction = reconstruction.concat(original);
	}
	//console.log(reconstruction);
	var e = {data: reconstruction};
	var spiralFunc = drawSpiral(x, y, b, samples, amplitude);
	spiralFunc(e);
}

/* get settings then fetch image */
function submitFunc(e) {
	e.preventDefault();
	form[10].innerHTML = '<i class="fa fa-cog fa-spin"></i> Draw!'
	if(form.elements[3].checked === true) {
		add = 1;
	} else {
		add = 0;
	}
	if(form.elements[9].checked === true) {
		full = 1;
	} else {
		full = 0;
	}
	
	radius = parseFloat(form.elements[6].value, 10);
	amplitude = parseFloat(form.elements[7].value, 10);
	sampleRate = parseFloat(form.elements[8].value, 10);
	storeSettings();

	if(fileInput.files[0] != undefined) {
		image.src = URL.createObjectURL(fileInput.files[0]);
		cors = true; // in case of error
	} else {
		image.src = form.elements[0].value;
		cors = false;
	}
}

function dLinkFunc(e) {
	e.preventDefault();
	var now = new Date();
	var string = now.toISOString();
	
	lDisplay.toBlob(function(blob) {
		saveAs(blob, 'linify_' + string.slice(0,10) + '_' + string.slice(11,13) + '-' + string.slice(14,16)+ '.png');
	});
}

function sLinkFunc(e) {
	if(shared === false) {
		sLink.innerHTML = '<i class="fa fa-cog fa-spin"></i> Save';
		shared = true;
		lDisplay.toBlob(function(blob) {
			var formData = new FormData();
			formData.append('add', add);
			formData.append('lineStep', lineStep);
			formData.append('width', cWidth);
			formData.append('height', cHeight);
			formData.append('size', blob.size);
			formData.append('image', blob);
			formData.append('lines', new Blob([lines], {type: 'application/octet-stream'}));
			
			var req = new XMLHttpRequest();
			
			req.onreadystatechange = function() {
				if(req.readyState === XMLHttpRequest.DONE) {
					if(req.status === 200) {
						window.location.href = '/' + req.responseText;
					} else {
						shared = false;
						sLink.innerHTML = '<i class="fa fa-cloud-upload></i> Save';
					}
				}
			}
			
			req.open('POST', '/u', true);
			req.send(formData);
		}, 'image/png');
	}
}

function sizeFunc(e) {
	size.style.display = 'none';
	rSize.style.display = 'inline-block';
	lDisplay.id = 'unBound';
	if(typeof rLink !== 'undefined') {
	}
}

function rSizeFunc(e) {
	rSize.style.display = 'none';
	size.style.display = 'inline-block';
	lDisplay.id = 'lCanvas';
	if(typeof rLink !== 'undefined') {
	}
}

function centerDraw(e) {
	var rect = this.getBoundingClientRect();
	var xPos = e.clientX - rect.left;
	var yPos = e.clientY - rect.top;
	
	cRatioX = xPos/rect.width;
	cRatioY = yPos/rect.height;
	var xC = Math.round(cRatioX * cCanvas.width);
	var yC = Math.round(cRatioY * cCanvas.height);
	
	cContext.clearRect(0,0,1024,1024);
	cContext.lineWidth = 2;
	cContext.strokeStyle = 'rgb(0,255,0)';
	cContext.beginPath();
	cContext.moveTo(xC, 0);
	cContext.lineTo(xC, 1024);
	cContext.stroke();
	cContext.beginPath();
	cContext.moveTo(0, yC);
	cContext.lineTo(1024, yC);
	cContext.stroke();
}

/* wipes canvas, fills in background color */
function prepCanvas() {
	cContext.clearRect(0,0,1024,1024);
	//lContext.clearRect(0,0,lDisplay.width, lDisplay.height);
	lContext.globalCompositeOperation = 'source-over';
	if(add) {		
		lContext.fillStyle = '#000000';
		lContext.fillRect(0,0,1024,1024);
	} else {
				
		lContext.fillStyle = '#FFFFFF';
		lContext.fillRect(0,0,1024,1024);
	}
}

/* tries to store color, add, numLines, lineStep, and testLines */
function storeSettings() {
	try {
		localStorage.setItem('radius', radius.toString());
		localStorage.setItem('amplitude', amplitude.toString());
		localStorage.setItem('sampleRate', sampleRate.toString());
		localStorage.setItem('add', add.toString());
		if(form.elements[2].checked === true) {
			var color = '1';
		} else {
			var color = '0';
		}
		localStorage.setItem('color', color);
		if(form.elements[9].checked === true) {
			var hiRes = '1';
		} else {
			var hiRes = '0';
		}
		localStorage.setItem('hiRes', hiRes);
	} catch(e) {
		console.log(e);
		return;
	}
}

function getSettings() {
	try {
		if(localStorage.length != 0) {
			document.getElementById('numLines').value = localStorage.getItem('radius');
			document.getElementById('lineStep').value = localStorage.getItem('amplitude');
			document.getElementById('testLines').value = localStorage.getItem('sampleRate');
			
			if(parseInt(localStorage.getItem('color'),10)) {
				form.elements[2].checked = true;
			} else {
				form.elements[4].checked = true;
			}
			if(parseInt(localStorage.getItem('add'),10)) {
				form.elements[3].checked = true;
			} else {
				form.elements[5].checked = true;
			}
			if(parseInt(localStorage.getItem('hiRes'),10)) {
				form.elements[9].checked = true;
			}
		}
	} catch(e) {
		console.log(e);
		return;
	}
}

/* Fisher-Yates Shuffle taken from http://stackoverflow.com/questions/2450954/how-to-randomize-shuffle-a-javascript-array */
function shuffle(array) {
	var currentIndex = array.length, temporaryValue, randomIndex;

	// While there remain elements to shuffle...
	while (0 !== currentIndex) {

		// Pick a remaining element...
		randomIndex = Math.floor(Math.random() * currentIndex);
		currentIndex -= 1;

		// And swap it with the current element.
		temporaryValue = array[currentIndex];
		array[currentIndex] = array[randomIndex];
		array[randomIndex] = temporaryValue;
	}

	return array;
}
